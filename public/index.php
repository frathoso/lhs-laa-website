<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Loads the website application
 */

require '../app/start.php';

?>
