  *****************************************************************************
  *                                                                           *
  *                        LOYOLA ALUMNI ASSOCIATION                          *
  *                                                                           *
  *****************************************************************************

      [2016] <website URL>
      All Rights Reserved.

   NOTICE:
   ------

   All information contained herein is, and remains the property of
   LOYOLA ALUMNI ASSOCIATION (LAA) and its official partners, if any.

   The intellectual and technical concepts contained herein are
   proprietary to LAA and its official partners and may be covered by
   Tanzanian and Foreign Patents, patents in process, and are protected
   by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained from
   LAA.
