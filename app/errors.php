<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Handles and overrides the default HTTP error behaviour
 */

/* 404 Not Found handler */
// Override the default 404 Not Found Handler with a custom handler
$container['notFoundHandler'] = function ($cont) {
  return new \LAA\Middleware\NotFoundHandler($cont->get('view'),
      function ($request, $response) use ($cont) {
          return $cont['response']
              ->withStatus(404);
    });
};

/* 405 Not Allowed handler */
// Override the default 405 Not Allowed Handler
$container['notAllowedHandler'] = function ($cont) {
    return function ($request, $response, $methods) use ($cont) {
        return $cont['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-type', 'text/html')
            ->write('<h1>Page not allowed</h1>');
    };
};

 ?>
