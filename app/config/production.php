<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines global configuration settings for production mode
 */

return [
  'app' => [
    'url' => ''
  ],

  'db' => [
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'name' => 'laa',
    'username' => '',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
  ],

  'auth' => [
    'session' => 'user_id',
    'remember' => 'user_r'
  ],

  'mail' => [
    'smtp_auth' => true,
    'smtp_secure' => 'tls',
    'host' => '',
    'username' => '',
    'password' => '',
    'port' => 587,
    'html' => true
  ],

  'twig' => [
    'debug' => false
  ],

  'csrf' => [
    'session' => 'csrf_token'
  ]
];

?>
