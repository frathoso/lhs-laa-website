<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines global configuration settings for development mode
 */

return [
  'app' => [
    'url' => 'http://www.laa.or.tz/'
  ],

  'db' => [
    'driver' => 'mysql',
    'host' => '127.0.0.1',
    'name' => 'laa',
    'username' => 'laa-user',
    'password' => 'laa-pass',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
  ],

  'auth' => [
    'session' => 'user_id',
    'remember' => 'user_r'
  ],

  'mail' => [
    'smtp_auth' => true,
    'smtp_secure' => 'tls',
    'host' => '',
    'username' => '',
    'password' => '',
    'port' => 587,
    'html' => true
  ],

  'twig' => [
    'debug' => true
  ],

  'csrf' => [
    'session' => 'csrf_token'
  ]
];

?>
