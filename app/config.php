<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Loads or sets up various website configurations
 */

// Grab application container
$container = $app->getContainer();

/*
 * Load application configurations from /app/config/ depending on the mode
 * defined in /mode.php
 */
$container['mode'] = trim(file_get_contents(DIR_ROOT.'/mode.php',
                          NULL, NULL, 0, 12));

$container['config'] = function () use($container){
   return new \Noodlehaus\Config(DIR_CONFIG.'/'.$container['mode'].'.php');
};

$container['isProduction'] = ($container['mode'] == MODE_PRODUCTION) ?
                              true : false;

// Sets up views templates
require_once ('views.php');

// Sets up all website routes
require_once ('routes.php');

// Sets up error handlers
require_once ('errors.php');

?>
