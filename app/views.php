<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Configures website views templates engine
 */

// Register Twig as the views templating engine
$container['view'] = function ($cont) use($container){
  $view = null;
  if($container['isProduction']){ // Cache during production only
    $view = new \Slim\Views\Twig(DIR_VIEWS, [
        'cache' => DIR_CACHE
    ]);
  }else{
    $view = new \Slim\Views\Twig(DIR_VIEWS, []);
  }

  $view->addExtension(new \Slim\Views\TwigExtension(
      $cont['router'],
      $cont['request']->getUri()
  ));

  return $view;

};

?>
