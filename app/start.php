<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Loads dependencies and starts the website application
 */

// Load global constatns
require_once ('names.php');

// Load all vendor dependencies
require_once (DIR_VENDOR.'/autoload.php');

// Define the website application
$app = new \Slim\App();

// Load application configurations
require_once (DIR_ROOT.'/app/config.php');

// Start the website application
$app->run();

?>
