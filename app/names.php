<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines all global constants and names used throughout the website
 */

/*
 * URL Names
 */
define ('URL_ABOUT',            'about');
define ('URL_ABOUT_CONTACT_US', 'about-contacts');
define ('URL_ABOUT_LAA',        'about-laa');
define ('URL_ABOUT_LEADERSHIP', 'about-leadership');
define ('URL_ABOUT_PRIVACY',    'about-privacy');
define ('URL_ABOUT_SUPPORT_US', 'about-support-us');
define ('URL_ABOUT_TERMS',      'about-terms');

define ('URL_ARCHIVES',         'archives');

define ('URL_EVENTS',          'events');
define ('URL_EVENTS_CALENDAR', 'events-calendar');
define ('URL_EVENTS_NEWS',     'events-news');

define ('URL_GET_INVOLVED',          'get-involved');
define ('URL_GET_INVOLVED_ALUMNI',   'get-involved-alumni');
define ('URL_GET_INVOLVED_STUDENTS', 'get-involved-students');

define ('URL_HOME',            'home');

define ('URL_PROGRAMMES',      'programmes');

define ('URL_SEARCH',          'search');

define ('URL_USER_LOGIN',      'user-login');
define ('URL_USER_REGISTER',   'user-register');


/*
 * Local Path Names for the application.
 */
define ('DIR_ROOT',   dirname(__DIR__.'/../../'));
define ('DIR_CACHE',  DIR_ROOT.'/app/cache');
define ('DIR_CONFIG', DIR_ROOT.'/app/config');
define ('DIR_LIBS',   DIR_ROOT.'/app/libs');
define ('DIR_PUBLIC', DIR_ROOT.'/public');
define ('DIR_ROUTES', DIR_ROOT.'/app/routes');
define ('DIR_VENDOR', DIR_ROOT.'/vendor');
define ('DIR_VIEWS',  DIR_ROOT.'/app/views');

/*
 * HTML templates names
 */
define ('TEMPLATE_ABOUT',            'about/about.html.twig');
define ('TEMPLATE_ABOUT_CONTACT_US', 'about/contact-us.html.twig');
define ('TEMPLATE_ABOUT_LAA',        'about/laa.html.twig');
define ('TEMPLATE_ABOUT_LEADERSHIP', 'about/leadership.html.twig');
define ('TEMPLATE_ABOUT_PRIVACY',    'about/privacy-statement.html.twig');
define ('TEMPLATE_ABOUT_SUPPORT_US', 'about/support-us.html.twig');
define ('TEMPLATE_ABOUT_TERMS',      'about/terms-of-use.html.twig');

define ('TEMPLATE_ARCHIVES', 'archives/archives.html.twig');

define ('TEMPLATE_EVENTS',          'events/events.html.twig');
define ('TEMPLATE_EVENTS_CALENDAR', 'events/calendar.html.twig');
define ('TEMPLATE_EVENTS_NEWS',     'events/news.html.twig');

define ('TEMPLATE_ERROR_404', 'error/404.html.twig');

define ('TEMPLATE_GET_INVOLVED',         'get-involved/get-involved.html.twig');
define ('TEMPLATE_GET_INVOLVED_ALUMNI',  'get-involved/get-involved-alumni.html.twig');
define ('TEMPLATE_GET_INVOLVED_STUDENTS','get-involved/get-involved-students.html.twig');

define ('TEMPLATE_HOME', 'home/home.html.twig');

define ('TEMPLATE_PROGRAMMES', 'programmes/programmes.html.twig');

define ('TEMPLATE_SEARCH', 'search/search.html.twig');

define ('TEMPLATE_USER_LOGIN',    'user/user-login.html.twig');
define ('TEMPLATE_USER_REGISTER', 'user/user-register.html.twig');

/*
 * Application modes
 */
define ('MODE_DEVELOPMENT', 'development');
define ('MODE_PRODUCTION',  'production');

?>
