<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Requires all the routes used throughout the website
 */

require_once (DIR_ROUTES.'/about.php');
require_once (DIR_ROUTES.'/archives.php');
require_once (DIR_ROUTES.'/events.php');
require_once (DIR_ROUTES.'/get-involved.php');
require_once (DIR_ROUTES.'/home.php');
require_once (DIR_ROUTES.'/programmes.php');
require_once (DIR_ROUTES.'/search.php');
require_once (DIR_ROUTES.'/user.php');

 ?>
