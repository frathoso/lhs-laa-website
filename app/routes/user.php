<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the user pages
 */

// Default user page redirects to the user login page
$app->get('/user', function($request, $response, $args) use ($app){
return $this->view->render($response, TEMPLATE_USER_LOGIN, []);
})->setName(URL_USER_LOGIN);

// Route for the user login page
$app->get('/user/login', function($request, $response, $args) use ($app){
 return $this->view->render($response, TEMPLATE_USER_LOGIN, []);
})->setName(URL_USER_LOGIN);

// Route for the user registration page
$app->get('/user/register', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_USER_REGISTER, []);
})->setName(URL_USER_REGISTER);

?>
