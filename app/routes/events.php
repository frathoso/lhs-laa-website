<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the events page for the website
 */

// Route for the default events page
$app->get('/events', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_EVENTS, []);
})->setName(URL_EVENTS);

// Route for the events calendar page
$app->get('/events/calendar', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_EVENTS_CALENDAR, []);
})->setName(URL_EVENTS_CALENDAR);

// Route for the events news page
$app->get('/events/news', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_EVENTS_NEWS, []);
})->setName(URL_EVENTS_NEWS);

?>
