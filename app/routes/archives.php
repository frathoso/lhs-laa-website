<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the archives page a.k.a the library page for the website
 */

// Route for the default archives page
$app->get('/archives', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ARCHIVES, []);
})->setName(URL_ARCHIVES);

?>
