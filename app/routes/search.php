<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the search pages
 */

// Default search page with an optional search query
$app->get('/search[/{query}]', function($request, $response, $args) use ($app){
  $params = [];

  if (isset($args['query'])) {
    $params['is_query'] = true;
    $params['query']    = $args['query'];
  }
  else {
    $params['is_query'] = false;
  }
return $this->view->render($response, TEMPLATE_SEARCH, $params);
})->setName(URL_SEARCH);

?>
