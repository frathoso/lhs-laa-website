<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the about page for the website
 */

// Route for the default about page
$app->get('/about', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT, []);
})->setName(URL_ABOUT);

// Route for the LAA official contacts page
$app->get('/about/contact-us', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_CONTACT_US, []);
})->setName(URL_ABOUT_CONTACT_US);

// Route for the description about LAA's mission and vision page
$app->get('/about/laa', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_LAA, []);
})->setName(URL_ABOUT_LAA);

// Route for the current LAA leadership
$app->get('/about/leadership', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_LEADERSHIP, []);
})->setName(URL_ABOUT_LEADERSHIP);

// Route for the LAA privacy statement page
$app->get('/about/privacy-statement', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_PRIVACY, []);
})->setName(URL_ABOUT_PRIVACY);

// Route for the LAA support us statement page
$app->get('/about/support-us', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_SUPPORT_US, []);
})->setName(URL_ABOUT_SUPPORT_US);

// Route for the LAA terms of website use page
$app->get('/about/terms-of-use', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_ABOUT_TERMS, []);
})->setName(URL_ABOUT_TERMS);

?>
