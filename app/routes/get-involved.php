<?php
/**
 * This file is subject to the terms and conditions defined in
 * file 'COPYRIGHT.txt', which is part of this source code package.
 *
 *	@author 	Francis Sowani (Frathoso) <frathoso@gmail.com>
 *	@version	1.0
 *
 *  Defines routes for the Get Involved page
 */

// Route for the default Get Involved page
$app->get('/get-involved', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_GET_INVOLVED, []);
})->setName(URL_GET_INVOLVED);

// Route for the alumni Get Involved page
$app->get('/get-involved/alumni', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_GET_INVOLVED_ALUMNI, []);
})->setName(URL_GET_INVOLVED_ALUMNI);

// Route for the current students Get Involved page
$app->get('/get-involved/students', function($request, $response, $args) use ($app){
  return $this->view->render($response, TEMPLATE_GET_INVOLVED_STUDENTS, []);
})->setName(URL_GET_INVOLVED_STUDENTS);

?>
